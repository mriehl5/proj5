"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
from pymongo import MongoClient
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import os

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controlsdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/controls")
@app.route("/controls/<int:ctrl_id>")
def controls(ctrl_id=None):
    items = [item for item in db.controls.find()]
    print(items)
    if ctrl_id == None:
        return flask.render_template('ctrl_list.html', items=items)

    elif ctrl_id >= len(items):
        return flask.render_template('404.html'), 404
        
    return flask.render_template('ctrl.html', item=items[ctrl_id])
    


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 1000, type=int)
    time = request.args.get('time', '')
    date = request.args.get('date', '')
    starting = arrow.get(f"{date}T{time}").isoformat()
    print(f"{time}\n{date}\n{starting}")
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, dist, starting)
    close_time = acp_times.close_time(km, dist, starting)
    print(f"{open_time}\n{close_time}")
    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)

@app.route("/_submit", methods=["POST"])
def _submit():
    """stores entries in a database"""

    app.logger.debug("Got a JSON request")
    req = request.get_json()

    if req["rows"][0][0] != "0":
        return flask.jsonify(error=True, message="First control should be at distance 0.0 i.e. the start")

    end = False
    filtered_rows = [req["rows"][0]]
    last_dist = int(req["rows"][0][0])
    for row in req["rows"][1:]:
        dist = row[0]
        if not end:
            if dist == "":
                end = True
            else:
                try:
                    dist_val = int(dist)
                except:
                    return flask.jsonify(error=True, message="Distace not a valid number")

                if dist_val <= last_dist:
                    return flask.jsonify(error=True, message="Control distances must be further than the last")
                else:
                    last_dist = dist_val
                    filtered_rows.append(row)
        else:
            if dist != "":
                return flask.jsonify(error=True, message="You have a gap in your control points")
        
    if last_dist != req["dist"]:
        return flask.jsonify(error=True, message="Final contol must equal overall brevet distance")
    
    req["rows"] = filtered_rows

    db.controls.insert_one(req)

    return flask.jsonify(error=False)

  

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
